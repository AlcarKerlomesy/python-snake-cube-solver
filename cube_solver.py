import numpy as np
import plotly.graph_objects as g_obj

# orignial_sequence:
# puzzle_desc = [3, 1, 2, 1, 1, 3, 1, 2, 1, 2, 1, 2] + [1]*8 + [2, 2] + [1]*5 + [2, 3, 1, 1, 1, 3, 1, 2] + [1]*9 + [3, 1]


class PuzzleSolver:

    def __init__(self, puzzle_desc):
        self.puzzle_desc = np.array(puzzle_desc, dtype=int)
        self.puzzle_state = [np.zeros((4, 4, 4), dtype=bool)]
        self.end_point = [np.zeros(3, dtype=int)]
        self.end_dir = [np.array([0, 1, 0], dtype=int)]
        self.end_dir_prev = [np.array([1, 0, 0], dtype=int)]
        self.coord_bound = [np.zeros(3, dtype=int)]
        self.sym_state = [True]
        self.puzzle_sol = [np.ones(len(puzzle_desc), dtype=int)*-1]
        self.cur_iter = 0

        self.puzzle_desc[0] -= 1

    def num_solution(self):
        return len(self.puzzle_sol)

    def next_iter(self):

        if self.cur_iter >= len(self.puzzle_desc):
            print('The puzzle is solved')
            return 0
        elif self.cur_iter == 0:
            self.puzzle_sol[0][self.cur_iter] = 0
            self.puzzle_state[0][:self.puzzle_desc[self.cur_iter]+1, 0, 0] = True
            self.end_point[0][0] = self.puzzle_desc[self.cur_iter]
            self.coord_bound[0][0] = self.end_point[0][0]
        elif self.cur_iter == 1:
            self.puzzle_sol[0][self.cur_iter] = 0
            self.puzzle_state[0][self.end_point[0][0], 1:self.puzzle_desc[self.cur_iter]+1, 0] = True
            self.end_point[0][1] = self.puzzle_desc[self.cur_iter]
            self.coord_bound[0][1] = self.end_point[0][1]
        else:
            num_old_sol = len(self.puzzle_sol)
            for m1 in range(num_old_sol):
                next_dir = - self.end_dir_prev[m1]
                for m2 in range(4):

                    if m2 > 0:
                        next_dir = np.cross(self.end_dir[m1], next_dir)

                    if self.sym_state[m1] and next_dir[2] < 0:
                        continue

                    dir_ind = next_dir.nonzero()[0].item()
                    cur_end_point = self.end_point[m1].copy()
                    next_end_point = cur_end_point + self.puzzle_desc[self.cur_iter]*next_dir
                    next_coord_bound = self.coord_bound[m1].copy()

                    if next_end_point[dir_ind] >= 4 or \
                            next_coord_bound[dir_ind] - next_end_point[dir_ind] >= 4:
                        continue

                    if next_end_point[dir_ind] < 0:
                        next_puzzle_state = np.roll(self.puzzle_state[m1], -next_end_point[dir_ind],
                                                    axis=dir_ind)
                        cur_end_point[dir_ind] -= next_end_point[dir_ind]
                        next_coord_bound[dir_ind] -= next_end_point[dir_ind]
                        next_end_point[dir_ind] = 0
                    else:
                        next_puzzle_state = self.puzzle_state[m1].copy()
                        next_coord_bound[dir_ind] = max(next_coord_bound[dir_ind],
                                                               next_end_point[dir_ind])

                    indices = list(cur_end_point)
                    if next_dir[dir_ind] == 1:
                        indices[dir_ind] = slice(cur_end_point[dir_ind] + 1,
                                                        next_end_point[dir_ind] + 1)
                    else:
                        indices[dir_ind] = slice(next_end_point[dir_ind],
                                                        cur_end_point[dir_ind])

                    if np.any(next_puzzle_state[tuple(indices)]):
                        continue

                    next_puzzle_state[tuple(indices)] = True

                    self.puzzle_state.append(next_puzzle_state)
                    self.end_point.append(next_end_point)
                    self.end_dir_prev.append(self.end_dir[m1].copy())
                    self.end_dir.append(next_dir)
                    self.coord_bound.append(next_coord_bound)
                    if self.sym_state[m1] and (m2 == 0 or m2 == 2):
                        self.sym_state.append(True)
                    else:
                        self.sym_state.append(False)
                    self.puzzle_sol.append(self.puzzle_sol[m1].copy())
                    self.puzzle_sol[-1][self.cur_iter] = m2

            self.puzzle_state = self.puzzle_state[num_old_sol:]
            self.end_point = self.end_point[num_old_sol:]
            self.end_dir = self.end_dir[num_old_sol:]
            self.end_dir_prev = self.end_dir_prev[num_old_sol:]
            self.coord_bound = self.coord_bound[num_old_sol:]
            self.sym_state = self.sym_state[num_old_sol:]
            self.puzzle_sol = self.puzzle_sol[num_old_sol:]

        self.cur_iter += 1
        return 1

    def show_solution(self, num_sol):

        if self.cur_iter == 0:
            print("Nothing to show")
            return

        if num_sol < 0 or num_sol >= len(self.puzzle_sol):
            print('"num_sol" is out of bound')
            return

        cur_dir = np.array((0, -1, 0), dtype=int)
        prev_dir = np.array((-1, 0, 0), dtype=int)
        lin_coord = np.zeros((3, self.cur_iter + 1), dtype=int)

        for m1 in range(self.cur_iter):
            next_dir = -prev_dir

            for m2 in range(self.puzzle_sol[num_sol][m1]):
                next_dir = np.cross(cur_dir, next_dir)

            lin_coord[:, m1+1] = lin_coord[:, m1] + self.puzzle_desc[m1]*next_dir
            prev_dir = cur_dir
            cur_dir = next_dir

        lin_coord = lin_coord + (self.end_point[num_sol] - lin_coord[:, -1]).reshape((3, 1))

        filled_coord = np.array(self.puzzle_state[num_sol].nonzero(), dtype=int)

        fig = g_obj.Figure(data=g_obj.Scatter3d(
            x=lin_coord[0], y=lin_coord[1], z=lin_coord[2],
            line=dict(
                color='darkblue',
                width=2
            ),
            mode="lines"
        ))

        fig.add_trace(g_obj.Scatter3d(
            x=filled_coord[0], y=filled_coord[1], z=filled_coord[2],
            marker=dict(
                size=4,
                color='green'
            ),
            mode="markers"
        ))

        fig.add_trace(g_obj.Scatter3d(
            x=[self.end_point[num_sol][0]], y=[self.end_point[num_sol][1]], z=[self.end_point[num_sol][2]],
            marker=dict(
                size=6,
                color='red'
            ),
            mode="markers"
        ))

        fig.show()


def show_solution(puzzle_desc, puzzle_sol):

    if puzzle_sol[0] == -1:
        print("Nothing to show")
        return

    cur_dir = np.array((0, -1, 0), dtype=int)
    prev_dir = np.array((-1, 0, 0), dtype=int)
    lin_coord = np.zeros((3, puzzle_sol.size + 1), dtype=int)

    m1 = 0

    while m1 < puzzle_sol.size and puzzle_sol[m1] >= 0:
        next_dir = -prev_dir

        for m2 in range(puzzle_sol[m1]):
            next_dir = np.cross(cur_dir, next_dir)

        lin_coord[:, m1+1] = lin_coord[:, m1] + puzzle_desc[m1]*next_dir
        prev_dir = cur_dir
        cur_dir = next_dir

        m1 += 1

    lin_coord[0, 0] += 1
    lin_coord = lin_coord[:, :m1+1]
    lin_coord = lin_coord - lin_coord.min(axis=1).reshape((3, 1))

    fig = g_obj.Figure(data=g_obj.Scatter3d(
        x=lin_coord[0], y=lin_coord[1], z=lin_coord[2],
        line=dict(
            color='darkblue',
            width=2
        ),
        mode="lines"
    ))

    fig.add_trace(g_obj.Scatter3d(
        x=[lin_coord[0, -1]], y=[lin_coord[1, -1]], z=[lin_coord[2, -1]],
        marker=dict(
            size=6,
            color='red'
        ),
        mode="markers"
    ))

    fig.show()


if __name__ == "__main__":
    p_desc = [3, 1, 2, 1, 1, 3, 1, 2, 1, 2, 1, 2] + [1] * 8 + [2, 2] + [1] * 5 + [2, 3, 1, 1, 1, 3, 1, 2] + [
        1] * 9 + [3, 1]
    a = PuzzleSolver(p_desc)
    while a.next_iter():
        pass

    # c1 = time() ; c = a.next_iter() ; c2 = time() ; print(c2 - c1)
    # after cur_iter = 16 (num. solutions: 141663 (iter. 16) -> 298301 (iter. 17)):
    # python : 47.36 sec
    # cython : 0.127 sec
