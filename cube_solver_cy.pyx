# cython: language_level=3, boundscheck=False, wraparound=False

import numpy as np
cimport numpy as np
import plotly.graph_objects as g_obj
cimport cython
from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free

cdef extern from "numpy/ndarraytypes.h":
    void PyArray_ENABLEFLAGS(np.PyArrayObject* arr, int flags)

np.import_array()

# original_sequence:
# puzzle_desc = [3, 1, 2, 1, 1, 3, 1, 2, 1, 2, 1, 2] + [1]*8 + [2, 2] + [1]*5 + [2, 3, 1, 1, 1, 3, 1, 2] + [1]*9 + [3, 1]

DTYPE = np.int8
ctypedef np.int8_t DTYPE_t

cdef size_t M_SIZE[7]

cdef int PUZZLE_STATE = 0
M_SIZE[PUZZLE_STATE] = 4 * 4 * 4

cdef int END_POINT = 1
M_SIZE[END_POINT] = 3

cdef int END_DIR = 2
M_SIZE[END_DIR] = 3

cdef int END_DIR_PREV = 3
M_SIZE[END_DIR_PREV] = 3

cdef int COORD_BOUND = 4
M_SIZE[COORD_BOUND] = 3

cdef int SYM_STATE = 5
M_SIZE[SYM_STATE] = 1

cdef int PUZZLE_SOL = 6
M_SIZE[PUZZLE_SOL] = -1 # Will be defined in the _PS_init

cdef int NUM_P = 7


cdef DTYPE_t** _PS_init(Py_ssize_t init_size, size_t len_puzzle_desc):

    cdef:
        int m1
        DTYPE_t** self = <DTYPE_t**> PyMem_Malloc(NUM_P * sizeof(DTYPE_t*))
    M_SIZE[PUZZLE_SOL] = len_puzzle_desc

    for m1 in range(NUM_P):
        self[m1] = <DTYPE_t*> PyMem_Malloc(init_size * M_SIZE[m1] * sizeof(DTYPE_t))

    return self

cdef size_t _PS_grow_size(DTYPE_t** self, size_t cur_size, size_t max_size):

        if cur_size <= max_size:
            return max_size

        cdef int m1
        max_size = max_size << 1

        for m1 in range(NUM_P):
            self[m1] = <DTYPE_t*> PyMem_Realloc(self[m1], max_size * M_SIZE[m1] * sizeof(DTYPE_t))

        return max_size

cdef class PuzzleSolver:

    cdef public:
        np.ndarray puzzle_desc
        np.ndarray puzzle_state
        np.ndarray end_point
        np.ndarray end_dir
        np.ndarray end_dir_prev
        np.ndarray coord_bound
        np.ndarray sym_state
        np.ndarray puzzle_sol
        int cur_iter

    def __cinit__(self, puzzle_desc):

        self.puzzle_desc = np.array(puzzle_desc, dtype=DTYPE)
        self.puzzle_state = np.zeros((1, 4, 4, 4), dtype=DTYPE)
        self.end_point = np.zeros((1, 3), dtype=DTYPE)
        self.end_dir = np.array([[0, 1, 0]], dtype=DTYPE)
        self.end_dir_prev = np.array([[1, 0, 0]], dtype=DTYPE)
        self.coord_bound = np.zeros((1, 3), dtype=DTYPE)
        self.sym_state = np.ones(1, dtype=DTYPE)
        self.puzzle_sol = np.ones((1, len(puzzle_desc)), dtype=DTYPE)*-1
        self.cur_iter = 0

        self.puzzle_desc[0] -= 1

    def num_solution(self):
        return self.puzzle_sol.shape[0]

    cdef void _reduce_size_and_copy(self, DTYPE_t** self_source, size_t cur_size):

        cdef:
            int m1
            np.npy_intp size_1d = cur_size
            np.npy_intp* size_2d = [cur_size, 3]
            np.npy_intp* size_4d = [cur_size, 4, 4, 4]

        for m1 in range(NUM_P):
            self_source[m1] = <DTYPE_t*> PyMem_Realloc(self_source[m1], cur_size * M_SIZE[m1] * sizeof(DTYPE_t))

        self.puzzle_state = np.PyArray_SimpleNewFromData(4, size_4d, np.NPY_INT8, self_source[PUZZLE_STATE])
        self.end_point = np.PyArray_SimpleNewFromData(2, size_2d, np.NPY_INT8, self_source[END_POINT])
        self.end_dir = np.PyArray_SimpleNewFromData(2, size_2d, np.NPY_INT8, self_source[END_DIR])
        self.end_dir_prev = np.PyArray_SimpleNewFromData(2, size_2d, np.NPY_INT8, self_source[END_DIR_PREV])
        self.coord_bound = np.PyArray_SimpleNewFromData(2, size_2d, np.NPY_INT8, self_source[COORD_BOUND])
        self.sym_state = np.PyArray_SimpleNewFromData(1, &size_1d, np.NPY_INT8, self_source[SYM_STATE])
        size_2d[1] = M_SIZE[PUZZLE_SOL]
        self.puzzle_sol = np.PyArray_SimpleNewFromData(2, size_2d, np.NPY_INT8, self_source[PUZZLE_SOL])

        PyArray_ENABLEFLAGS(<np.PyArrayObject*> self.puzzle_state, np.NPY_OWNDATA)
        PyArray_ENABLEFLAGS(<np.PyArrayObject*> self.end_point, np.NPY_OWNDATA)
        PyArray_ENABLEFLAGS(<np.PyArrayObject*> self.end_dir, np.NPY_OWNDATA)
        PyArray_ENABLEFLAGS(<np.PyArrayObject*> self.end_dir_prev, np.NPY_OWNDATA)
        PyArray_ENABLEFLAGS(<np.PyArrayObject*> self.coord_bound, np.NPY_OWNDATA)
        PyArray_ENABLEFLAGS(<np.PyArrayObject*> self.sym_state, np.NPY_OWNDATA)
        PyArray_ENABLEFLAGS(<np.PyArrayObject*> self.puzzle_sol, np.NPY_OWNDATA)

        PyMem_Free(self_source)

    cpdef int next_iter(self):

        if self.cur_iter >= len(self.puzzle_desc):
            print('The puzzle is solved')
            return 0

        if self.cur_iter == 0:
            self.puzzle_sol[0, self.cur_iter] = 0
            self.puzzle_state[0, :self.puzzle_desc[self.cur_iter]+1, 0, 0] = 1
            self.end_point[0, 0] = self.puzzle_desc[self.cur_iter]
            self.coord_bound[0, 0] = self.end_point[0, 0]

            self.cur_iter += 1
            return 1

        if self.cur_iter == 1:
            self.puzzle_sol[0, self.cur_iter] = 0
            self.puzzle_state[0, self.end_point[0, 0], 1:self.puzzle_desc[self.cur_iter]+1, 0] = 1
            self.end_point[0, 1] = self.puzzle_desc[self.cur_iter]
            self.coord_bound[0, 1] = self.end_point[0, 1]

            self.cur_iter += 1
            return 1

        cdef:
            DTYPE_t[:] puzzle_desc = self.puzzle_desc
            DTYPE_t[:,:] self_puzzle_state = self.puzzle_state.reshape((self.puzzle_state.shape[0], 64))
            DTYPE_t[:,:] self_end_point = self.end_point, self_end_dir = self.end_dir
            DTYPE_t[:,:] self_end_dir_prev = self.end_dir_prev, self_coord_bound = self.coord_bound
            DTYPE_t[:] self_sym_state = self.sym_state
            DTYPE_t[:,:] self_puzzle_sol = self.puzzle_sol

            DTYPE_t next_dir[3]
            DTYPE_t next_dir_temp[3]
            DTYPE_t* next_puzzle_state
            DTYPE_t* roll_puzzle_state
            DTYPE_t* next_end_point
            DTYPE_t* next_coord_bound
            DTYPE_t[:] cur_puzzle_state, cur_end_dir

            bint not_a_solution
            int m1, m2, m3, m4, dir_ind, roll_shift, end_point_ind
            int* stride = [64, 16, 4, 1]

            size_t num_new_sol = 0, max_new_sol = 1 << 10
            DTYPE_t** new_self = _PS_init(max_new_sol, puzzle_desc.shape[0])

        for m1 in range(self_puzzle_sol.shape[0]):

            cur_puzzle_state = self_puzzle_state[m1]
            cur_end_dir = self_end_dir[m1]

            for m2 in range(4):

                max_new_sol = _PS_grow_size(new_self, num_new_sol + 1, max_new_sol)

                next_puzzle_state = new_self[PUZZLE_STATE] + num_new_sol * M_SIZE[PUZZLE_STATE]
                next_end_point = new_self[END_POINT] + num_new_sol * M_SIZE[END_POINT]
                next_coord_bound = new_self[COORD_BOUND] +  num_new_sol * M_SIZE[COORD_BOUND]

                if m2 == 0:
                    for m3 in range(3):
                        next_dir[m3] = - self_end_dir_prev[m1, m3]
                else:
                    next_dir_temp[0] = cur_end_dir[1]*next_dir[2] - cur_end_dir[2]*next_dir[1]
                    next_dir_temp[1] = cur_end_dir[2]*next_dir[0] - cur_end_dir[0]*next_dir[2]
                    next_dir_temp[2] = cur_end_dir[0]*next_dir[1] - cur_end_dir[1]*next_dir[0]
                    for m3 in range(3):
                        next_dir[m3] = next_dir_temp[m3]

                if self_sym_state[m1] and next_dir[2] < 0:
                    continue

                for m3 in range(3):
                    if next_dir[m3]:
                        dir_ind = m3
                        break
                for m3 in range(3):
                    next_end_point[m3] = self_end_point[m1, m3]
                    next_coord_bound[m3] = self_coord_bound[m1, m3]
                end_point_ind = 16*next_end_point[0] + 4*next_end_point[1] + next_end_point[2]
                next_end_point[dir_ind] += puzzle_desc[self.cur_iter]*next_dir[dir_ind]

                if next_end_point[dir_ind] >= 4 or next_coord_bound[dir_ind] - next_end_point[dir_ind] >= 4:
                    continue

                if next_end_point[dir_ind] < 0:
                    roll_shift = - stride[dir_ind+1]*next_end_point[dir_ind]
                    roll_puzzle_state = next_puzzle_state + roll_shift
                    for m3 from 0 <= m3 < 64 by stride[dir_ind]:
                        for m4 in range(roll_shift):
                            next_puzzle_state[m3 + m4] = 0
                        for m4 in range(stride[dir_ind] - roll_shift):
                            roll_puzzle_state[m3 + m4] = cur_puzzle_state[m3 + m4]
                    end_point_ind += roll_shift
                    next_coord_bound[dir_ind] -= next_end_point[dir_ind]
                    next_end_point[dir_ind] = 0
                else:
                    for m3 in range(64):
                        next_puzzle_state[m3] = cur_puzzle_state[m3]
                    next_coord_bound[dir_ind] = next_coord_bound[dir_ind] if \
                        next_coord_bound[dir_ind] > next_end_point[dir_ind] else next_end_point[dir_ind]

                not_a_solution = False

                for m3 in range(puzzle_desc[self.cur_iter]):
                    end_point_ind += next_dir[dir_ind]*stride[dir_ind+1]
                    if next_puzzle_state[end_point_ind] == 1:
                        not_a_solution = True
                        break
                    next_puzzle_state[end_point_ind] = 1

                if not_a_solution:
                    continue

                for m3 in range(3):
                    new_self[END_DIR_PREV][num_new_sol * M_SIZE[END_DIR_PREV] + m3] = cur_end_dir[m3]
                    new_self[END_DIR][num_new_sol * M_SIZE[END_DIR] + m3] = next_dir[m3]
                new_self[SYM_STATE][num_new_sol] = self_sym_state[m1] and (m2 == 0 or m2 == 2)
                for m3 in range(M_SIZE[PUZZLE_SOL]):
                    new_self[PUZZLE_SOL][num_new_sol * M_SIZE[PUZZLE_SOL] + m3] = self_puzzle_sol[m1, m3]
                new_self[PUZZLE_SOL][num_new_sol * M_SIZE[PUZZLE_SOL] + self.cur_iter] = m2

                num_new_sol += 1

        self._reduce_size_and_copy(new_self, num_new_sol)

        self.cur_iter += 1
        return 1

    def show_solution(self, num_sol):

        if self.cur_iter == 0:
            print("Nothing to show")
            return

        if num_sol < 0 or num_sol >= len(self.puzzle_sol):
            print('"num_sol" is out of bound')
            return

        cur_dir = np.array((0, -1, 0), dtype=int)
        prev_dir = np.array((-1, 0, 0), dtype=int)
        lin_coord = np.zeros((3, self.cur_iter + 1), dtype=int)

        for m1 in range(self.cur_iter):
            next_dir = -prev_dir

            for m2 in range(self.puzzle_sol[num_sol][m1]):
                next_dir = np.cross(cur_dir, next_dir)

            lin_coord[:, m1+1] = lin_coord[:, m1] + self.puzzle_desc[m1]*next_dir
            prev_dir = cur_dir
            cur_dir = next_dir

        lin_coord = lin_coord + (self.end_point[num_sol] - lin_coord[:, -1]).reshape((3, 1))

        filled_coord = np.array(self.puzzle_state[num_sol].nonzero(), dtype=int)

        fig = g_obj.Figure(data=g_obj.Scatter3d(
            x=lin_coord[0], y=lin_coord[1], z=lin_coord[2],
            line=dict(
                color='darkblue',
                width=2
            ),
            mode="lines"
        ))

        fig.add_trace(g_obj.Scatter3d(
            x=filled_coord[0], y=filled_coord[1], z=filled_coord[2],
            marker=dict(
                size=4,
                color='green'
            ),
            mode="markers"
        ))

        fig.add_trace(g_obj.Scatter3d(
            x=[self.end_point[num_sol][0]], y=[self.end_point[num_sol][1]], z=[self.end_point[num_sol][2]],
            marker=dict(
                size=6,
                color='red'
            ),
            mode="markers"
        ))

        fig.show()


def show_solution(puzzle_desc, puzzle_sol):

    if puzzle_sol[0] == -1:
        print("Nothing to show")
        return

    cur_dir = np.array((0, -1, 0), dtype=int)
    prev_dir = np.array((-1, 0, 0), dtype=int)
    lin_coord = np.zeros((3, puzzle_sol.size + 1), dtype=int)

    m1 = 0

    while m1 < puzzle_sol.size and puzzle_sol[m1] >= 0:
        next_dir = -prev_dir

        for m2 in range(puzzle_sol[m1]):
            next_dir = np.cross(cur_dir, next_dir)

        lin_coord[:, m1+1] = lin_coord[:, m1] + puzzle_desc[m1]*next_dir
        prev_dir = cur_dir
        cur_dir = next_dir

        m1 += 1

    lin_coord[0, 0] += 1
    lin_coord = lin_coord[:, :m1+1]
    lin_coord = lin_coord - lin_coord.min(axis=1).reshape((3, 1))

    fig = g_obj.Figure(data=g_obj.Scatter3d(
        x=lin_coord[0], y=lin_coord[1], z=lin_coord[2],
        line=dict(
            color='darkblue',
            width=2
        ),
        mode="lines"
    ))

    fig.add_trace(g_obj.Scatter3d(
        x=[lin_coord[0, -1]], y=[lin_coord[1, -1]], z=[lin_coord[2, -1]],
        marker=dict(
            size=6,
            color='red'
        ),
        mode="markers"
    ))

    fig.show()
